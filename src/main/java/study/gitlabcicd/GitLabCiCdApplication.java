package study.gitlabcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitLabCiCdApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitLabCiCdApplication.class, args);
    }

}
