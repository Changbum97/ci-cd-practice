package study.gitlabcicd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import study.gitlabcicd.domain.entity.Visit;

public interface VisitRepository extends JpaRepository<Visit, Long> {
}
